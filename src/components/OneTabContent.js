import React from "react";

const OneTabContent = (props) => (
  <div>
      {props.content}
  </div>
);

export default OneTabContent;