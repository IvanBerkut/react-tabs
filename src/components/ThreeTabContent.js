import React from "react";

const ThreeTabContent = (props) => (
  <div>
      {props.content}
  </div>
);

export default ThreeTabContent;