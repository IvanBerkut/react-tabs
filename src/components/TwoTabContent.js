import React from "react";

const TwoTabContent = (props) => (
  <div>
      {props.content}
  </div>
);

export default TwoTabContent;