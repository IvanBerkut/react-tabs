import React from "react";
import PropTypes from "prop-types";

const TabManager = ({activeTab, handleTab, tabs, children}) => {
  return (
    <>
      <div className="tab-manager">
        {tabs.map(({ label, value }) => (
          <div
            key={label}
            className={`tab ${value === activeTab ? "selected-tab" : ""}`}
            onClick={() => {
              handleTab(value);
            }}
          >
            {label}
          </div>
        ))}
      </div>
      <div className="tab-content">
        {children}
      </div>
    </>
  )
}

TabManager.propTypes = {
  activeTab: PropTypes.number.isRequired,
  handleTab: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(Object).isRequired,
};

export default TabManager;