import React, { useState } from 'react';
import TabManager from './components/TabManager';
import OneTabContent from './components/OneTabContent';
import TwoTabContent from './components/TwoTabContent';
import ThreeTabContent from './components/ThreeTabContent';
import './App.css';

const TABS = [
  { label: "Tab One", value: 1, content: 'Content of tab One'},
  { label: "Tab Two", value: 2, content: 'Some Content Here'},
  { label: "Tab Three", value: 3, content: 'Third tab content'}
];
export default function App() {
  const [activeTab, handleTab] = useState(1);
  const currentContent = TABS.find(i => i.value === activeTab).content
  return (
    <div className="App">
      <TabManager tabs={TABS} activeTab={activeTab} handleTab={handleTab}>
        {activeTab === 1 && <OneTabContent content={currentContent} />}
        {activeTab === 2 && <TwoTabContent content={currentContent} />}
        {activeTab === 3 && <ThreeTabContent content={currentContent} />}  
      </TabManager>
    </div>
  );
}
